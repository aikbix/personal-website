Bare Bones Blog
===============

So simple that you don't even need tests!

About
-----
A blogging web application written with Tornado and Pymongo. Very minimalistic
with no styles in templates and only the essential request handlers defined.

Licensed under the MIT License.


Installation
------------

```
virtualenv -p /path/to/python3 .env
source .env/bin/activate
pip install -r requirements.txt
```
