
###############################################################################
# Licensing and credits
###############################################################################


__author__ = "Ian Kinsey"
__license__ = "MIT"


###############################################################################
# Imports
###############################################################################


import tornado.ioloop
from cryptacular.bcrypt import BCRYPTPasswordManager
from pymongo import MongoClient
from bson.objectid import ObjectId
from PyRSS2Gen import RSS2, RSSItem, Guid
from datetime import datetime
from os import urandom
from os.path import join, dirname
from lxml.etree import Element, tostring
#from tornado.template import Loader
from jinja2 import Environment, FileSystemLoader
from tornado.web import (Application, authenticated, RequestHandler,
                         StaticFileHandler)


global rss_feed
global sitemap


###############################################################################
# Metadata
###############################################################################


title = "Aikbix"
description = ''
link = 'aikbix.com'
site = 'http://{}'.format(link)


###############################################################################
# Useful Functions
###############################################################################


def update_blog_status():
    '''
    Updates sitemap and rss feed with new content.
    '''

    global rss_feed
    global sitemap

    rss_feed = gen_rss_feed()
    sitemap = gen_sitemap()


def get_posts(page_num):
    '''
    Return paginated list of blog posts.
    '''

    return db.posts.find().sort('timestamp', -1).skip(page_num * 10).limit(10)


def gen_url(year, month, slug):
    '''
    Return url of post.
    '''

    return "http://{}/blog/{}/{}/{}".format(link, year, month, slug)


def gen_slug(title):
    '''
    Return slug from title.
    '''

    return title.strip().replace(' ', '-').lower()[:60]


def gen_rss_item(page):
    '''
    Generate rss item for feed.
    '''

    link = gen_url(page['year'], page['month'], page['slug'])

    return RSSItem(
        title=page['title'],
        link=link,
        guid=Guid(link),
        description=page['body']
    )


def gen_rss_feed():
    '''
    Generate an RSS feed.
    '''
    return RSS2(
        title=title,
        description=description,
        link=link,
        lastBuildDate=datetime.now(),
        ttl=60,
        items=[gen_rss_item(page) for page in get_posts(0)]
    ).to_xml()


def gen_xml_item(name, content):
    item = Element(name)
    item.text = content

    return item


def gen_sitemap_item(loc, priority='.5', changefreq='monthly', lastmod=None):
    priority = str(priority)

    url_elem = Element('url')

    # loc
    url_elem.append(gen_xml_item('loc', loc))
    url_elem.append(gen_xml_item('priority', priority))
    url_elem.append(gen_xml_item('changefreq', changefreq))

    if lastmod:
        mod_date = datetime.strftime(lastmod, "%Y-%m-%d")
        url_elem.append(gen_xml_item('lastmod', mod_date))

    return url_elem


def gen_sitemap():
    '''
    Generate XML sitemap.
    '''
    urlset = Element(
        'urlset',
        xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
    )

    urlset.append(
        gen_sitemap_item(
            site,
            priority=1,
            changefreq='daily',
            lastmod=datetime.now()
        ))

    urlset.append(
        gen_sitemap_item(
            site + '/about',
            priority=1,
            changefreq='monthly',
            lastmod=datetime.now()
        ))

    for post in db.posts.find():
        urlset.append(
            gen_sitemap_item(
                gen_url(post['year'], post['month'], post['slug']),
                lastmod=post['timestamp']
            ))

    return tostring(
        urlset,
        xml_declaration=True,
        encoding="UTF-8",
    )


def gen_template_env(path='templates'):
    '''
    Return jinja2 template environment for specified path.
    '''
    loader = FileSystemLoader([
        join(dirname(__file__), 'templates')
    ])

    env = Environment(loader=loader, trim_blocks=True)

    return env


###############################################################################
# Base Request Handler
###############################################################################


class Page(RequestHandler):
    '''
    Base class for pages.
    '''
    jinja_env = gen_template_env()

    def get_current_user(self):
        '''
        Overrides default get_current_user to enable authentication.
        '''
        return self.get_secure_cookie('username')

    def render(self, name, *args, **kwargs):
        '''
        Overrides Tornado's default render method. Replaces Tornado templates
        with jinja2 templates.
        '''
        self.write(self.jinja_env.get_template(name).render(
            # Pass the xsrf value in explicitly.
            year=datetime.now().year,
            xsrf=self.xsrf_form_html(),
            *args,
            **kwargs
        ))

    def argument(self, name):
        '''
        Returns argument from name, defaults to an empty string  (False) if
        there is no argument.
        '''

        return self.get_argument(name, default='')

    def fail(self, message):
        '''
        Sends failure message
        '''

        self.write(message)

###############################################################################
# Pages
###############################################################################


class PostList(Page):
    '''
    List of blog posts.
    '''

    def get(self, page_num=1):
        index = int(page_num) - 1
        next = index + 1

        self.render(
            'postlist',
            page_num=page_num,
            posts=get_posts(index),
            next=next if list(get_posts(next)) else None
        )


class AboutPage(Page):
    '''
    "About Me" page.
    '''

    def get(self):
        self.render('about')


class AdminLogin(Page):
    '''
    Logs in the administrator.
    '''

    def get(self):
        self.render('login')

    def post(self):
        username = self.argument('username')
        password = self.argument('password')

        if username and password:
            db_user = db.admins.find_one({'username': username})

            if db_user:
                if pw_manager.check(db_user['password'], password):
                    self.set_secure_cookie('username', username)
                    self.redirect('/admin')
                else:
                    self.fail('Invalid password')
            else:
                self.fail('User does not exist')
        else:
            self.fail()


class Admin(Page):
    '''
    Administration panel.
    '''

    @authenticated
    def get(self):
        return self.render('admin')


class NewPost(Page):
    '''
    Create a new post.
    '''

    @authenticated
    def get(self):
        return self.render('new_post')

    @authenticated
    def post(self):
        username = self.get_secure_cookie('username')
        title = self.argument('title')
        tags = self.argument('tags')
        body = self.argument('body')

        if username and title and tags and body:
            now = datetime.now()
            db.posts.insert({
                'title': title,
                'body': body,
                'tags': tags.split(','),  # TODO, make this more dynamic
                'username': username,
                'year': now.year,
                'month': now.month,
                'slug': gen_slug(title),
                'timestamp': now,
                'comments': [],
                'total_comments': 0
            })

            update_blog_status()
            self.write('Post successful')
        else:
            self.fail()


class RemovePost(Page):
    '''
    Remove blog post.
    '''

    @authenticated
    def get(self):
        posts = db.posts.find()

        return self.render('remove_post', posts=posts)

    @authenticated
    def post(self):
        id = self.argument('post_id')

        if id:
            db.posts.remove({
                '_id': ObjectId(id)
            })
            update_blog_status()

            self.write('Remove succeeded')
        else:
            self.fail()


class RemoveUser(Page):
    '''
    Remove administrator.
    '''

    @authenticated
    def get(self):
        users = db.admins.find()
        self.render('remove_user', users=users)

    @authenticated
    def post(self):
        username = self.argument('username')

        if username:
            if db.admins.find_one({'username': username}):
                db.admins.remove({'username': username})
                self.fail('User successfully removed.')
            else:
                self.fail('User does not exist.')
        else:
            self.fail()


class NewUser(Page):
    '''
    Create a new administrator.
    '''

    @authenticated
    def get(self):
        self.render('new_user')

    @authenticated
    def post(self):
        username = self.argument('username')
        password = self.argument('password')
        verify = self.argument('verify')

        if username and password:
            if password == verify:
                if not db.admins.find_one({'username': username}):
                    db.admins.insert({
                        'username': self.argument('username'),
                        'password': pw_manager.encode(password)
                    })
                    self.write('Successfully created user')
                else:
                    self.fail('User exists')
            else:
                self.fail('Passwords do not match')
        else:
            self.fail()


class NewComment(Page):
    '''
    Add a comment to a post.
    '''

    def post(self, year, month, slug):
        title = self.argument('title')
        email = self.argument('email')
        body = self.argument('body')
        name = self.argument('name')

        if body and name:
            db.posts.update({
                'year': int(year),
                'month': int(month),
                'slug': slug,
            }, {
                '$push': {
                    'comments': {
                        '_id': ObjectId(),
                        'name': name,
                        'email': email,
                        'title': title,
                        'body': body,
                        'timestamp': datetime.now(),
                    }}})

            self.redirect('/blog/{}/{}/{}'.format(year, month, slug))
        else:
            self.fail()


class RemoveComment(Page):
    '''
    Remove a comment from a post.
    '''

    @authenticated
    def post(self):
        post_id = self.argument('post_id')
        comment_id = self.argument('comment_id')

        if post_id and comment_id:
            query = {'$pull': {'comments': {'_id': ObjectId(comment_id)}}}
            db.posts.update({'_id': ObjectId(post_id)}, query)
            self.write("Comment removed successfully.")
        else:
            self.fail()


class BlogEntry(Page):
    '''
    Single blog entry with comments.
    '''

    def get(self, year, month, slug):
        self.render(
            'entry',
            post=db.posts.find_one({
                'year': int(year),
                'month': int(month),
                'slug': slug
            }),
            admin=self.get_current_user()
        )


class RssFeed(Page):
    '''
    RSS feed.
    '''

    def get(self):
        self.set_header("Content-Type", "application/xhtml+xml; charset=UTF-8")
        self.write(rss_feed)


class Sitemap(Page):
    '''
    XML sitemap for website.
    '''

    def get(self):
        self.set_header("Content-Type", "application/xml; charset=UTF-8")
        self.write(sitemap)


class Robots(Page):
    '''
    robots.txt
    '''

    def get(self):
        self.set_header("Content-Type", "text/plain; charset=UTF-8")
        self.write((
            "User-agent: * \n"
            "Disallow: \n"
            "Sitemap: http://aikbix.com/sitemap.xml"
        ))


###############################################################################
# Application configuration
###############################################################################


# Password Manager
pw_manager = BCRYPTPasswordManager()

# Database connection
conn = MongoClient()
db = conn['aikbix-personal-blog']

# Cached RSS feed
rss_feed = gen_rss_feed()

# Cached sitemap
sitemap = gen_sitemap()

# Current directory
base = dirname(__file__)

# Application settings
tornado_settings = {
    #'template_path': join(base, 'templates'),
    'cookie_secret': urandom(64),
    'login_url': '/admin/login',
    'xsrf_cookies': True
}

# Tornado Handlers
tornado_handlers = [
    (r'/static/(.*)', StaticFileHandler, {'path': join(base, 'static')}),
    (r'/', PostList),
    (r'/robots.txt', Robots),
    (r'/sitemap.xml', Sitemap),
    (r'/admin', Admin),
    (r'/admin/login', AdminLogin),
    (r'/admin/new_post', NewPost),
    (r'/admin/new_user', NewUser),
    (r'/admin/remove_post', RemovePost),
    (r'/admin/remove_user', RemoveUser),
    #(r'/admin/remove_comment', RemoveComment),
    (r'/about', AboutPage),
    (r'/rss', RssFeed),
    (r"/page/(\d+)", PostList),
    (r"/blog/(\d+)/(\d+)/(.*)", BlogEntry),
    #(r"/comment/(\d+)/(\d+)/(.*)", NewComment),
]

application = Application(tornado_handlers, **tornado_settings)

if __name__ == '__main__':
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
